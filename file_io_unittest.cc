//
// Copyright (C) 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <dhcp_client/file_io.h>

#include <string>

#include <base/files/file_util.h>
#include <base/files/scoped_temp_dir.h>
#include <gtest/gtest.h>

#include <shill/net/byte_string.h>

namespace dhcp_client {
namespace {
  const unsigned char kFakeFileContent[] = "fake_content";
}

class FileIOTest : public testing::Test {
 public:
  FileIOTest() {}
};


TEST_F(FileIOTest, PathExistsTrue) {
  base::ScopedTempDir temp_dir;
  ASSERT_TRUE(temp_dir.CreateUniqueTempDir());
  base::FilePath path;
  ASSERT_TRUE(base::CreateTemporaryFileInDir(temp_dir.path(), &path));
  EXPECT_TRUE(FileIO::GetInstance()->PathExists(path.value()));
}

TEST_F(FileIOTest, PathExistsFalse) {
  const std::string kNonExistentFilePath("non-existent_path");
  EXPECT_FALSE(FileIO::GetInstance()->PathExists(kNonExistentFilePath));
}

TEST_F(FileIOTest, DeleteFile) {
  base::ScopedTempDir temp_dir;
  ASSERT_TRUE(temp_dir.CreateUniqueTempDir());
  base::FilePath path;
  ASSERT_TRUE(base::CreateTemporaryFileInDir(temp_dir.path(), &path));
  EXPECT_TRUE(FileIO::GetInstance()->DeleteFile(path.value()));
}

TEST_F(FileIOTest, WriteAndReadFile) {
  base::ScopedTempDir temp_dir;
  ASSERT_TRUE(temp_dir.CreateUniqueTempDir());
  base::FilePath path;
  ASSERT_TRUE(base::CreateTemporaryFileInDir(temp_dir.path(), &path));
  shill::ByteString content =
      shill::ByteString(kFakeFileContent, sizeof(kFakeFileContent));
  EXPECT_TRUE(FileIO::GetInstance()->Write(path.value(), content));
  shill::ByteString content_read;
  EXPECT_TRUE(FileIO::GetInstance()->Read(path.value(), &content_read));
  EXPECT_TRUE(content.Equals(content_read));
}

TEST_F(FileIOTest, ReadEmptyFile) {
  base::ScopedTempDir temp_dir;
  ASSERT_TRUE(temp_dir.CreateUniqueTempDir());
  base::FilePath path;
  ASSERT_TRUE(base::CreateTemporaryFileInDir(temp_dir.path(), &path));
  shill::ByteString content_read;
  EXPECT_TRUE(FileIO::GetInstance()->Read(path.value(), &content_read));
  EXPECT_EQ(0, content_read.GetLength());
}

}  // namespace dhcp_client
